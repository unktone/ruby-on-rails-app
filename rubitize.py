"""
Remediate Dependency vulnerabilities
"""
import sys
import os
import json
import gitlab
import re

def gitlab_auth():
  token_gl = os.environ.get("token_gl")
  gl = gitlab.Gitlab(private_token=token_gl)
  print("[LOG] rubitize.py: authenticated")

def import_report():
  with open("scan-report.json", "r") as file:
    report_json = json.loads(file.read())
    print("[LOG] import_report: gemnsasium scan report imported")
    return report_json

def get_criticals(report):
  updates = []
  for i in report["vulnerabilities"]:
    if i["location"]["file"] == "src/Gemfile.lock":
      remediation = i["solution"]
      package_name = i["location"]["dependency"]["package"]["name"]
      package_version = i["location"]["dependency"]["version"]
      new_version_raw = str(re.findall("\d\.\d\.\d\ or above", remediation))
      new_version = re.findall("\d\.\d\.\d", new_version_raw)
      new_version = new_version[0]
      with open("update_commands.txt", "a") as file:
        command = f"bundle update {package_name}"
        file.write("\n")
        file.write(command)
      print(f"[LOG] get_criticals: Package Name: {package_name}")
      print(f"[LOG] get_criticals: Package Version: {package_version}")
      print(f"[LOG] get_criticals: Remediation: {remediation}")
      print(f"[LOG] get_criticals: New Version: {new_version}")

def main():
  gitlab_auth()
  report = import_report()
  get_criticals(report)
  with open("update_commands.txt", "r") as file:
    print_this = file.read()
    print(print_this)
if __name__ == '__main__':
  main()
