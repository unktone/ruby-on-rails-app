# Self-Healing Image Build Pipeline

This is an image-build pipeline designed to keep it's own dependencies up-to-date, and continuously publish
updated images to consume for containerized workloads.

The idea hatched itself after sitting down and building a portion of the Case Study Pipeline.  From there I realized, ok, if the assumption is EKS, I can create a self-healing published image, under a stable tag, which could continuously be consumed by self-healing container workloads (i.e. new container spins up under the pod, and pulls image:stable, while since the last pull, image: stable may have updated itself.)

## Project-Specific Operations

* Some sort of disclaimer

* I was unable to start a codefresh trial on Monday, MLK Jr day, so I went with gitlab to accomodate CI/CD functionallity
* I don't know Ruby and Ruby on Rails, so the version and dependency management portion of this automation is written by me, but uses 'bundler'.  'Bundler' just facilitiates the dependency update - and it looks like a lot more good stuff that I didn't have time to explore.
* The magic is in the remediation-plan stage where we take the results of the gemnasium scan, and decide what to do with it.
* Since I was running out of time I wanted to spend, there are many areas of improvement on this auto-healing dependency management - especially from a Gem and Ruby on Rails perspective, that I simply did not want to get into for this - so my logic ("rubitize.py") is only identifying elements of dependencies present in the gemnasium scan, that are within the Gemfile.

## Flow
![Diagram of Flow](docs/flow-wm.png?raw=true "Diagram of Flow")

## Flow and products
![Diagram of Logos](docs/logo_diagram.png?raw=true "Diagram of Flow")

# Pipeline Stages

### test
1.) Test: in this stage we run gemnasium and the results are stored in a temporary report: gl-dependency-scanning-report.json
    - This report creates visuals in the Merge Request UI, but since this is a self-sustaining automation, we don't care about the UI, so we could store this as any artifact we want, and pass it to the subsequent stages.

### remediate-plan
2.) remediate-plan:  Stage 2 runs our "rubitize.py" script in order to derive actionble information from the Gemnasium Dependency scan.  After refactoring throughout the project progression, this script really only interacts with the scan results file, and doesn't actually need gitlab api access

### remediate-exe
3.) remediate-exe:  This is where we're actually taking the action on remediation of Dependency versions, and another are that would require a lot of grooming over time to meet specific needs.  In our example, we're running 'bundle update gem' commands, as well as commiting the changes to a bugfix/remediation branch in git.

### remediate-mr
4.) remediate-mr: This is where we create the Merge Request(Pull Request), and merge the request to our default branch through the gitlab API.

### build
5.) build: Finally, we build our image using the updated codebase for the image-build after the MR/PR - and we are in theory, free of dependency vulnerabilities, for items in the gemfile.

For open source projects, say how it is licensed.

## Project status
To Do - imminent: 1.) finish/fix git config in build container - this would allow the pipeline to create the new branch with the remediations(why not done: I didn't feel like getting into building custom build images for this project - so I attempted to configure git at runtime, but wasted too much time, so it's place holder)
2.) Tailor and tweak Ruby on Rails dependency update strategy.  I don't know much about managing dependencies for ruby on rails, so my pipeline focuses on dependencies which are in the gemfile, and does a blanket update on those - without checking versions or testing - so those would be things to figure out.'
3.) Code for the stable tag to be moved to the new image build