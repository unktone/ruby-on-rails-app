"""
Create MR for ruby gem remediation
"""
import sys
import os
import json
import gitlab
import re

def gitlab_auth():
  token_gl = os.environ.get("token_gl")
  gl = gitlab.Gitlab(private_token=token_gl)
  print("[LOG] rubitize.py: authenticated")
  return gl

def create_mr(gl):
  project_id = 32903606
  project = gl.projects.get(project_id)
  mr = project.mergerequests.create({'source_branch': 'bugfix/remediation',
                                   'target_branch': 'master',
                                   'title': 'automatic remediation'})
  return mr

def merge_mr(mr):
  mr.merge()
  print("[LOG] merge_mr: Merge Request has been merged into main branch")
def main():
  gl = gitlab_auth()
  mr = create_mr(gl)
  merge_mr(mr)
if __name__ == '__main__':
  main()
